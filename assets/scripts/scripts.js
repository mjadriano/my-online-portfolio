// Navbar Menubar responsive
const toggle = document.getElementById("bars");
toggle.addEventListener("click", function () {
  const navbar = document.getElementById("navbar");
  navbar.classList.toggle("active");
});
// close of menubar in navbar

// Start of the Text in landing page

const phrases = [
  "I am",
  "MJ",
  "a Web Developer",
  "and",
  "an Electronics Engineer",
];

const el = document.querySelector(".text");
const fx = new TextScramble(el);
let counter = 0;
const next = function () {
  fx.setText(phrases[counter]).then(function () {
    setTimeout(next, 1000);
  });
  counter = (counter + 1) % phrases.length;
};
next();


//JavaScript Document